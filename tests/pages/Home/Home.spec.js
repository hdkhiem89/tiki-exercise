import React from 'react'
import { Home } from 'pages/Home/Home'
import { shallow } from 'enzyme'
import sinon from 'sinon'

describe('(Page) Home', () => {
  let _component;
  let props;

  beforeEach(() => {
    props = {
      getMovies: sinon.spy(),
      movies: [{
        id: '2110',
        thumbnail: 'thumbnail.jpg',
        name: 'HUNG THẦN ĐẠI DƯƠNG: THẢM SÁT',
        rating_code: 'C16',
        rating_icon: 'rating_icon.png',
      }]
    }
    _component = shallow(<Home {...props}/>)
  })

  it('should trigger getMovies when componentDidMount', () => {
    expect(props.getMovies).to.have.been.called(1);
  })

  it('should render correctly', () => {
    expect(_component.find('MoviesCollection')).to.have.lengthOf(1);
  })

  it('should render spinner when props.movies has not loaded yet', () => {
    const props = {
      getMovies: () => {},
      movies: null,
    };
    const component = shallow(<Home {...props}/>);
    expect(component.find('Spinner')).to.have.lengthOf(1);
  })
})
