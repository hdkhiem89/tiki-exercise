import React from 'react'
import { Detail } from 'pages/Detail/Detail'
import { DetailMovie } from 'components/DetailMovie/DetailMovie'
import { shallow } from 'enzyme'
import sinon from 'sinon'

describe('(Page) Detail', () => {
  let _component;
  let props;
  let id = '2110';

  beforeEach(() => {
    props = {
      getDetailMovie: sinon.spy(),
      match: {
        params: { id }
      },
      selectedMovie: {
        id,
        name: 'HUNG THẦN ĐẠI DƯƠNG: THẢM SÁT',
        thumbnail: 'thumbnail.jpg',
        full_description: 'Trong chuyến phiêu lưu đến một vùng đảo ít người biết đến, 4 cô gái Mia (Sophie Nélisse), Sasha (Corinne Foxx), Nicole (Sistine Stallone) và Alexa (Brianne Tju) ...',
        genre: 'Hồi hộp, Kinh Dị, Phiêu Lưu',
        rating_code: 'C16',
        rating_icon: 'icon.png',
        release_date: '2019-09-27 00:00:00',
        movie_director: 'Johannes Roberts',
        movie_actress: 'Sophie Nélisse, Corinne Foxx, Brianne Tju, Sistine Stallone',
      }
    }
    _component = shallow(<Detail {...props}/>)
  })

  it('should trigger getDetailMovie when componentDidMount', () => {
    expect(props.getDetailMovie).to.have.been.called(1);
  })

  it('should load correct movie by Id', () => {
    expect(props.getDetailMovie).to.have.been.called.with(id);
  })

  it('should render correctly', () => {
    expect(_component.find('DetailMovie')).to.have.lengthOf(1);
  })

  it('should render spinner when props.movies has not loaded yet', () => {
    const props = {
      getDetailMovie: () => {},
      selectedMovie: null,
      match: {
        params: { id }
      },
    };
    const component = shallow(<Detail {...props}/>);
    expect(component.find('Spinner')).to.have.lengthOf(1);
  })
})
