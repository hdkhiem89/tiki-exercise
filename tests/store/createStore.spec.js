import {
  default as createStore
} from 'store/createStore'

describe('(Store) createStore', () => {
  let store

  before(() => {
    store = createStore()
  })

  it('should have an empty asyncReducers object', () => {
    expect(store.asyncReducers).to.be.an('object')
    expect(store.asyncReducers).to.be.empty()
  })

  describe('(HomeReducer)', () => {
    it('store should be initialized with HomeReducer state', () => {
      const homeReducer = {
        movies: [ {id: 1}, {id: 2}, {id: 3}]
      }
      store.dispatch({
        type    : 'GET_MOVIES_SUCCEEDED',
        payload : homeReducer.movies
      })
      expect(store.getState().homeReducer).to.eql(homeReducer)
    })
  })

  describe('(DetailReducer)', () => {
    it('store should be initialized with DetailReducer state', () => {
      const detailReducer = {
        selectedMovie: { id: 1 }
      }
      store.dispatch({
        type    : 'GET_DETAIL_MOVIE_SUCCEEDED',
        payload : detailReducer.selectedMovie
      })
      expect(store.getState().detailReducer).to.eql(detailReducer)
    })
  })
})
