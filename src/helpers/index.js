import { FORMAT_DATE } from 'const';
import moment from 'moment';

export const formatDate = (date) => {
  let formatedDate = '';
  if (date) {
    formatedDate = moment().format(FORMAT_DATE);
  }

  return formatedDate;
}