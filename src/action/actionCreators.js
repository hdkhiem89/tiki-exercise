import {
    GET_MOVIES_REQUESTED,
    GET_DETAIL_MOVIE_REQUESTED,
    GET_DETAIL_MOVIE_SUCCEEDED,
    CLEAR_SELECTED_MOVIE,
    CLEAR_ERROR,
} from './actionTypes';

export function getMovies() {
    return {
      type: GET_MOVIES_REQUESTED,
    }
  }

export function getDetailMovie(id) {
    return {
        type: GET_DETAIL_MOVIE_REQUESTED,
        payload: id
    }
}

export function openModal(movie) {
    return {
        type: GET_DETAIL_MOVIE_SUCCEEDED,
        payload: movie
    }
}

export function clearSelectedMovie() {
    return {
        type: CLEAR_SELECTED_MOVIE,
    }
}


export function clearError() {
    return {
        type: CLEAR_ERROR,
    }
}