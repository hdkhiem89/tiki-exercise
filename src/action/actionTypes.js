export const CLEAR_SELECTED_MOVIE = 'CLEAR_SELECTED_MOVIE'
export const CLEAR_ERROR = 'CLEAR_ERROR'

export const GET_MOVIES_REQUESTED = 'GET_MOVIES_REQUESTED'
export const GET_MOVIES_SUCCEEDED = 'GET_MOVIES_SUCCEEDED'
export const GET_MOVIES_FAILED = 'GET_MOVIES_FAILED'

export const GET_DETAIL_MOVIE_REQUESTED = 'GET_DETAIL_MOVIE_REQUESTED'
export const GET_DETAIL_MOVIE_SUCCEEDED = 'GET_DETAIL_MOVIE_SUCCEEDED'
export const GET_DETAIL_MOVIE_FAIL = 'GET_DETAIL_MOVIE_FAIL'
