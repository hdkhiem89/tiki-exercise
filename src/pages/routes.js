import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import CoreLayout from '../layouts/PageLayout/PageLayout'
import Home from './Home';
import Detail from './Detail';

function Routes() {
  return (
    <Router>
      <Switch>
        <CoreLayout >
          <Route exact path='/' component={Home} />
          <Route path="/detail/:id" component={Detail} />
        </CoreLayout>
      </Switch>
    </Router>
  );
}

export default Routes;
