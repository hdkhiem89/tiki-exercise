import {
  GET_DETAIL_MOVIE_SUCCEEDED,
} from 'action/actionTypes'

export const SELECTED_MOVIE = 'selectedMovie';

const appReducer = (state = {}, action) => {
  switch (action.type) {
  case GET_DETAIL_MOVIE_SUCCEEDED:
    return {
        ...state,
        selectedMovie: action.payload
    };
  default:
    return state;
  }
};

export default appReducer;
