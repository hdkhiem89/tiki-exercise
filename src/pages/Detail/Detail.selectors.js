import { createSelector } from 'reselect';
import { SELECTED_MOVIE } from './Detail.reducer';


const getDetailMovieSelector = (state) => state.detailReducer[SELECTED_MOVIE];

export default createSelector(
  getDetailMovieSelector,
  item => item,
)