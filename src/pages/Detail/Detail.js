import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getDetailMovie } from 'action/actionCreators'
import PropTypes from 'prop-types';
import Spinner from 'components/Spinner/Spinner';
import DetailMovie from 'components/DetailMovie/DetailMovie';
import detailMovieSelector from './Detail.selectors';
import './Detail.scss';

export class Detail extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getDetailMovie(id);
  }

  render() {
    const { selectedMovie } = this.props;

    return (
      <div>
        <nav aria-label="breadcrumb">
          <ol className="breadcrumb">
            <li className="breadcrumb-item"><a href="/">Home</a></li>
            <li className="breadcrumb-item active" aria-current="detail">Detail</li>
          </ol>
        </nav>
        { selectedMovie ? <DetailMovie selectedMovie={selectedMovie}/> : <div className='text-center'><Spinner /></div> }
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedMovie: detailMovieSelector(state),
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getDetailMovie,
    },
    dispatch,
  );

Detail.propTypes = {
  selectedMovie: PropTypes.object,
  getDetailMovie: PropTypes.func.isRequired,
}

Detail.defaultProps = {
  selectedMovie: null,
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
