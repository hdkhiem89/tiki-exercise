import { createSelector } from 'reselect';
import { MOVIES } from './Home.reducer';


const getMoviesSelector = (state) => state.homeReducer[MOVIES];

export default createSelector(
  getMoviesSelector,
  items => items && items.map(item => {
    const { showing_date_time } = item.languages && item.languages[0].sessions && item.languages[0].sessions[0];
    return { ...item, showing_date_time };
  }),
)