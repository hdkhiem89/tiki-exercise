import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getMovies } from 'action/actionCreators'
import { MOVIES } from 'store/appReducer';
import PropTypes from 'prop-types';
import MoviesCollection from 'components/MoviesCollection/MoviesCollection';
import Spinner from 'components/Spinner/Spinner';
import moviesSelector from './Home.selectors'; 
import './Home.scss';

export class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedMovie: {},
    };
  }

  componentDidMount() {
    this.props.getMovies();
  }

  render() {
    const { movies } = this.props;
    if (!movies) return (<div className='text-center'><Spinner /></div>);

    return (
      <div className='text-center'>
        <MoviesCollection
          movies={movies}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    movies: moviesSelector(state),
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getMovies,
    },
    dispatch,
  );

Home.propTypes = {
  movies: PropTypes.array,
  getMovies: PropTypes.func.isRequired,
}

Home.defaultProps = {
  movies: null,
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
