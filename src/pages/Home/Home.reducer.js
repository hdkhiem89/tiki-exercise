import {
  GET_MOVIES_SUCCEEDED,
} from 'action/actionTypes'

export const MOVIES = 'movies';

const homeReducer = (state = {}, action) => {
  switch (action.type) {
  case GET_MOVIES_SUCCEEDED:
    return {
        ...state,
        movies: action.payload
    };
  default:
    return state;
  }
};

export default homeReducer;
