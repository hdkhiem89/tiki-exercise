import React from 'react'
import { IndexLink, Link } from 'react-router'
import PropTypes from 'prop-types'
import Header from 'components/Header/Header';
import Footer from 'components/Footer/Footer';
import './PageLayout.scss'

export const PageLayout = ({ children }) => (
  <div className='container'>
    <div className='page-layout__viewport'>
      <Header />
      {children}
      <Footer />
    </div>
  </div>
)
PageLayout.propTypes = {
  children: PropTypes.node,
}

export default PageLayout
