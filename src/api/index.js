import axios from 'axios';
import {
    DETAIL_MOVIE_URI,
    MOVIES_URI
} from 'const/uri'

export function getDetailMovieApi(id) {
    return axios.get(`${DETAIL_MOVIE_URI}/${id}`);
}

export function getMoviesApi() {
    return axios.get(MOVIES_URI);
}
