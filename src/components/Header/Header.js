import React from 'react';

const Header = () => (
  <div className='mb-5 text-center'>
    <img className='logo' src="https://www.cgv.vn/skin/frontend/cgv/default/images/cgvlogo.png" alt="CGV" border="0"></img>
  </div>
)

export default Header;