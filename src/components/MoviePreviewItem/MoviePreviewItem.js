import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { formatDate } from 'helpers';
import './MoviePreviewItem.scss';

const MoviePreviewItem = ({ id, name, thumbnail, rating_code, rating_icon, showing_date_time }) => (
  <li className='list-group-item list-group-item-action text-left'>
    <Link to={`/detail/${id}`}>
      <div className='row'>
          <div className='col-sm-12 col-md-3 m-auto text-center'>
            <img className='thumbnail' src={thumbnail} />
          </div>
          <div className='col-sm-12 col-md-9'>
            <h5>{name}</h5>
            <p>
              <span>Rating: <img src={rating_icon} alt={rating_code}/> </span>
            </p>
            <p>
              <span>Ngày khởi chiêu: {formatDate(showing_date_time)} </span>
            </p>
          </div>
        </div>
    </Link>
  </li>
)

MoviePreviewItem.propTypes = {
  name: PropTypes.string,
  thumbnail: PropTypes.string,
  rating_code: PropTypes.string,
  rating_icon: PropTypes.string,
  showing_date_time: PropTypes.string,
}

MoviePreviewItem.defaultProps = {
  name: '',
  thumbnail: '',
  rating_code: '',
  rating_icon: '',
  showing_date_time: '',
}

export default MoviePreviewItem;