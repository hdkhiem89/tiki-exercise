import React from 'react';
import PropTypes from 'prop-types';
import MoviePreviewItem from 'components/MoviePreviewItem/MoviePreviewItem';

const MoviesCollection = ({ movies }) => (
  <ul className='list-group mb-5'>
    { movies.map(a =>
      <MoviePreviewItem
        key={a.id}
        id={a.id}
        thumbnail={a.thumbnail}
        name={a.name}
        rating_code={a.rating_code}
        rating_icon={a.rating_icon}
        showing_date_time={a.showing_date_time}
      />
      ) }
  </ul>
)

MoviesCollection.propTypes = {
  movies: PropTypes.array,
}

MoviesCollection.defaultProps = {
  movies: [],
}

export default MoviesCollection;