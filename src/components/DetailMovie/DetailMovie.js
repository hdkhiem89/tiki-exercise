import React, { Fragment } from "react";
import PropTypes from "prop-types";
import { formatDate } from "helpers";
import "./DetailMovie.scss";

const DetailMovie = ({ selectedMovie }) => (
  <Fragment>
    <div className="row">
      <div className="col-12">
        <h4>{selectedMovie.name}</h4>
      </div>
    </div>
    <div className="row text-center">
      <div className="col-12">
        <img className="detail-thumbnail" src={selectedMovie.thumbnail} />
      </div>
    </div>
    <div className="row">
      <div className="col-12 detail-description">
        <p>
          <span>
            Ngày chiếu: <span>{formatDate(selectedMovie.release_date)}</span>
          </span>
        </p>
        <p>
          <span>
            Thể loại: <span>{selectedMovie.genre}</span>
          </span>
        </p>
        <p>
          <span>
            Rating:  
            <img
              className="detail-rate-icon"
              src={selectedMovie.rating_icon}
              alt={selectedMovie.rating_code}
            />
          </span>
        </p>
        <p>
          <span>
            Diễn Viên: <span>{selectedMovie.movie_director}</span>
          </span>
        </p>
        <p>
          <span>
            Diễn Viên: <span>{selectedMovie.movie_actress}</span>
          </span>
        </p>
        <p>
          <span>Mô Tả:</span>
          <p>{selectedMovie.full_description}</p>
        </p>
      </div>
    </div>
  </Fragment>
);

DetailMovie.propTypes = {
  selectedMovie: PropTypes.object
};

DetailMovie.defaultProps = {
  selectedMovie: null
};

export default DetailMovie;
