import React from 'react';
import './Footer.scss';

const Footer = () => (
  <div className='mb-5 text-center'>
    <hr/>
    <p className='font-weight-bold'>CÔNG TY TNHH CJ CGV VIETNAM</p>
    <pre>COPYRIGHT 2017 CJ CGV. All RIGHTS RESERVED .</pre>
  </div>
)

export default Footer;