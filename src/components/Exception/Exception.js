import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { clearError } from 'action/actionCreators';
import { ERROR } from 'store/appReducer';

import './Exception.scss';

class Exception extends PureComponent {
  constructor () {
    super();
    this.state = {
      showModal: false
    };
    
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }s
  
  handleCloseModal () {
    this.setState({ showModal: false });
    this.props.clearError();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({ showModal: true });
    } else {
      this.setState({ showModal: false });
    }
  }

  render () {
    const error = this.props.error || '';

    return (
      <Modal
        isOpen={this.state.showModal}
        onRequestClose={this.handleCloseModal}
        shouldCloseOnOverlayClick={true}
        className='exception'
      >
        <button type="button" className="close" aria-label="Close" onClick={this.handleCloseModal}>
          <span aria-hidden="true">&times;</span>
        </button>
        <h4>Error!</h4>
        <p>{error}</p>
      </Modal>
    )
  }
}

Exception.propTypes = {
  error: PropTypes.string,
}

Exception.defaultProps = {
  error: '',
}

const mapStateToProps = state => {
  return {
    error: state.appReducer[ERROR],
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      clearError,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(Exception);