import { call, put, takeEvery, takeLatest, fork } from 'redux-saga/effects'
import {
    GET_MOVIES_REQUESTED,
    GET_MOVIES_SUCCEEDED,
    GET_MOVIES_FAILED,
    GET_DETAIL_MOVIE_REQUESTED,
    GET_DETAIL_MOVIE_SUCCEEDED,
    GET_DETAIL_MOVIE_FAIL,
 } from 'action/actionTypes'
import { getDetailMovieApi, getMoviesApi } from '../api'

function* getMovies(action) {
    try {
       const response = yield call(getMoviesApi, action.payload);
       if (response.data.errors && response.data.errors.length > 0){
         yield put({type: GET_MOVIES_FAILED, payload: response.data.errors[0].detail});
       } else {
         yield put({type: GET_MOVIES_SUCCEEDED, payload: response.data.data[0].movies});
       }
    } catch (e) {
       yield put({type: GET_MOVIES_FAILED, payload: e.message});
    }
 }
 
 function* watchGetMovies() {
   yield takeLatest(GET_MOVIES_REQUESTED, getMovies);
 }

 function* getDetailMovie(action) {
   try {
      const response = yield call(getDetailMovieApi, action.payload);
      if (response.data.errors && response.data.errors.length > 0){
         yield put({type: GET_DETAIL_MOVIE_FAIL, payload: response.data.errors[0].detail});
      } else {
         yield put({type: GET_DETAIL_MOVIE_SUCCEEDED, payload: response.data.data});
      }
   } catch (e) {
      yield put({type: GET_DETAIL_MOVIE_FAIL, payload: e.message});
   }
}

function* watchDetailGetMovie() {
  yield takeLatest(GET_DETAIL_MOVIE_REQUESTED, getDetailMovie);
}

export function* mySaga() {
    yield fork(watchGetMovies);
    yield fork(watchDetailGetMovie);
}

export default mySaga;
