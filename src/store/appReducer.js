import {
  GET_MOVIES_FAILED,
  GET_DETAIL_MOVIE_FAIL,
} from 'action/actionTypes'

export const ERROR = 'error';

// General app state
const appReducer = (state = {}, action) => {
  switch (action.type) {
  case GET_MOVIES_FAILED:
    return {
      ...state,
      error: action.payload
    };
  case GET_DETAIL_MOVIE_FAIL:
    return {
        ...state,
        error: action.payload
      };
  default:
    return state;
  }
};

export default appReducer;
