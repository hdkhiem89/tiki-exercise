import { combineReducers } from 'redux'
import appReducer from './appReducer';
import homeReducer from 'pages/Home/Home.reducer';
import detailReducer from 'pages/Detail/Detail.reducer';

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    appReducer,
    homeReducer,
    detailReducer,
    ...asyncReducers
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
