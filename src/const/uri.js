import moment from 'moment';

export const HOST ='https://www.cgv.vn/default/api'
export const DETAIL_MOVIE_URI = `${HOST}/movie/movie/id`;
export const MOVIES_URI = `${HOST}/cinema/showtimes/id/022/date/${moment().add(1, 'd').format('DDMMYYYY')}`;
