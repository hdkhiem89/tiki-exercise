# Tiki exercise
Implement a simple CGV web app.

## Run project
yarn start

## Run unit test
yarn test
yarn test:watch

## yarn build
yarn build

## yarn lint
Lints the project for potential errors
yarn lint

## Project Structure
```
.
├── build                    # All build-related code
├── public                   # Static public assets (not imported anywhere in source code)
├── server                   # Express application that provides webpack middleware
│   └── main.js              # Server application entry point
├── src                      # Application source code
│   ├── index.html           # Main HTML page container for app
│   ├── main.js              # Application bootstrap and rendering
│   ├── normalize.js         # Browser normalization and polyfills
│   ├── components           # Global Reusable Components
│   ├── layouts              # Components that dictate major page structure
│   │   └── PageLayout       # Global application layout in which to render routes
│   ├── pages                # Main route definitions and async split points
│   │   ├── index.js         # Bootstrap main application routes with store
│   │   ├── Page             # Fractal route
│   │   │   ├── index.js     # Route definitions and async split points
│   │   │   ├── selector     # Selector of Home page
│   │   │   ├── *.scss       # Styling
│   │   │   └── reducer **   # Reducer of this page
│   ├── store                # Redux-specific pieces
│   │   ├── createStore.js   # Create and instrument redux store
│   │   └── appReducers.js   # General application reducer
│   └── styles               # Application-wide styles (generally settings)
└── tests                    # Unit tests
```
